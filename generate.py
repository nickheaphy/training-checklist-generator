from openpyxl import load_workbook
import os
import fnmatch
import sys
import datetime

training_file = "Training.xlsx"
stub_file = "*_stub.html"

DEBUG = False


# -----------------------------------------------------------
def main(training, stub):
    '''Main Program Loop'''

    options_html = ""
    jscript_html = ""
    training_html = ""

    print("Processing...")
    print(training)
    print(stub)

    
    # Should really refactor this, it is a horrid hack and my eyes hurt
    # when I look at it...

    # ----------------------------------------
    # Open the Excel file -----------------
    wb = load_workbook(filename = training)
    
    # TRAINING DAYS SECTION -----------------------
    ws = wb['TrainingDays']
    trainingidarray = []
    options_html += "<h4>Training</h4>" + os.linesep 
    for i, row in enumerate(ws.iter_rows(min_row = ws.min_row, max_row = ws.max_row)):
        #skip the first row
        if i == 0:
            continue
        the_id = row[0].value
        description = row[1].value
        default = row[2].value
        options_html += f"<label><input type='checkbox' name='{the_id}cb' id='{the_id}cb' class='check' "
        if default == "Y":
            options_html += "CHECKED "
        options_html += "onclick='toggle_visibility();'/>"
        options_html += f"{description}</label>" + os.linesep
        trainingidarray.append(the_id)
    
    
    # HARDWARE AND SOFTWARE OPTIONS SECTION -----------------------
    ws = wb['MachineOptions']
    lastgroup = ""
    idarray = []
    for i, row in enumerate(ws.iter_rows(min_row = ws.min_row, max_row = ws.max_row)):
        #skip the first row
        if i == 0:
            continue

        group = row[0].value
        the_id = row[1].value
        description = row[2].value
        default = row[3].value

        if group != lastgroup:
            # this is a new group
            options_html += f"<h4>{group}</h4>" + os.linesep 

        options_html += f"<label><input type='checkbox' name='{the_id}cb' id='{the_id}cb' class='check' "
        if default == "Y":
            options_html += "CHECKED "
        options_html += "onclick='toggle_visibility(\"\");'/>"
        options_html += f"{description}</label>" + os.linesep

        idarray.append(the_id)
        lastgroup = group


    # checkbox settings
    # this builds some javascript to get all the checkbox settings
    for the_id in trainingidarray:
        jscript_html += f"var {the_id} = document.getElementById('{the_id}cb').checked ? true : false; " + os.linesep
    for the_id in idarray:
        jscript_html += f"var {the_id} = document.getElementById('{the_id}cb').checked ? true : false; " + os.linesep




    # TRAINING ITEMS SECTION -----------------
    ws = wb['TrainingItems']
    lastgroup = ""
    moduleopen = False
    itemlist = []
    signofftext = "<tr class='subrow'><td class='subrow'>Sign Off</td><td class='subrow'>Staff Initials</td><td class='subrow'></td></tr>" + os.linesep
    for i, row in enumerate(ws.iter_rows(min_row = ws.min_row, max_row = ws.max_row)):
        #skip the first row
        if i == 0:
            continue

        item = {
            "modulename": row[0].value,
            "component": row[1].value,
            "trainingdepend": row[2].value.split(' ') if row[2].value is not None else [],
            "configdepend": row[3].value.split(' ') if row[3].value is not None else []
        }

        # Dirty hack to validate
        for i in item['trainingdepend']:
            if i not in trainingidarray:
                print(f"There is a training dependany '{i}'' that is not on the training page {trainingidarray}")
                print(f"Please fix and reprocess")
                exit()

        for i in item['configdepend']:
            if i not in idarray:
                print(f"There is a training dependany '{i}' that is not on the config page {idarray}")
                print(f"Please fix and reprocess")
                exit()

        itemlist.append(item)

        #Headers
        if item['modulename'] is not None and item['modulename'][0] == ">":
            if moduleopen:
                # close module
                training_html += signofftext + "</table>" + os.linesep
                moduleopen = False
            training_html += f"<h5 class='c{len(itemlist)-1}'"
            training_html += f">{item['modulename'][1:]}</h5>" + os.linesep
        
        #Modules
        if item['modulename'] is not None and item['modulename'] != "" and item['modulename'][0] != ">" and item['modulename'][0] != "*":
            #new module
            if moduleopen:
                training_html += signofftext + "</table>" + os.linesep
                moduleopen = False
            moduleopen = True
            training_html += f"<table class='c{len(itemlist)-1}'>" + os.linesep 
            training_html += "<tr>" + os.linesep
            training_html += f"<th colspan='4'>{item['modulename']}</th></tr>" + os.linesep
            training_html += "<tr class='subrow'><td class='subrow'>Component</td><td class='subrow withpad' colspan='2'>Covered</td></tr>" + os.linesep

        # Activities
        if item['modulename'] is not None and item['modulename'][0] == "*":
            training_html += "<tr class='subrow'><td colspan='4' class='subrow'"
            training_html += f" class='c{len(itemlist)-1}'"
            training_html += f">{item['modulename'][1:-1]}</td></tr>" + os.linesep

        # Training Components
        if item['component'] is not None and item['component'] != "":
            training_html += f"<tr class='c{len(itemlist)-1}'>"
            # do we need to indent?
            if item['component'][0] == ".":
                if item['component'][1:1] == ".":
                    #double indent
                    training_html += f"<td><span class='indent2'>{item['component'][2:]}</span></td><td colspan='2'></td></tr>" + os.linesep
                else:
                    training_html += f"<td><span class='indent'>{item['component'][1:]}</span></td><td colspan='2'></td></tr>" + os.linesep
            else:
                training_html += f"<td>{item['component']}</td><td colspan='2'></td></tr>" + os.linesep

    #Finish, close the table
    training_html += signofftext + "</table>" + os.linesep


    # Now back to the Javascript Logic for showing and hiding as we needed the training items
    # and their dependencies to build this.
    for i,item in enumerate(itemlist):
        # This is going to be ugly. Why did I think it was a good idea to use Python to generate Javascript?
        if len(item['trainingdepend']) > 0 or len(item['configdepend']) > 0:
            jscript_html += "if ("

            if len(item['trainingdepend']) > 0:
                jscript_html += "("
                for trainc in item['trainingdepend']:
                    jscript_html += trainc + " || "
                jscript_html = jscript_html[:-4] + ") "
                if len(item['configdepend']) > 0:
                    jscript_html += "&& "
            
            if len(item['configdepend']) > 0:
                jscript_html += "("
                for configc in item['configdepend']:
                    jscript_html += configc + " || "
                jscript_html = jscript_html[:-4] + ")"
            
            jscript_html += ") {" + os.linesep
            jscript_html += f"$('.c{i}').show();" + os.linesep
            jscript_html += "} else {" + os.linesep
            jscript_html += f"$('.c{i}').hide();" + os.linesep
            jscript_html += "}" + os.linesep

    
    # Now inject all this HTML and Javascript into the stub file
    # INSERT_BUILDDATE_HERE
    # INSERT_JAVASCRIPT_HERE
    # INSERT_OPTIONS_HERE
    # INSERT_TRAINING_HERE

    d = datetime.datetime.today()

    s = open(stub).read()
    s = s.replace('INSERT_BUILDDATE_HERE', str(d))
    s = s.replace('INSERT_JAVASCRIPT_HERE', jscript_html)
    s = s.replace('INSERT_OPTIONS_HERE', options_html)
    s = s.replace('INSERT_TRAINING_HERE', training_html)
    
    # write the final file out (remove the stub from the filename)
    f = open(stub.replace("_stub.html", ".html"),"w")
    f.write(s)
    f.close()
    # Finished. Don't look at this ugly code again.

# -----------------------------------------------------------
def findfile(pattern, path):
    '''Find a list of files that match the pattern in the path'''
    result = []
    for root, dirs, files in os.walk(path):
        for name in files:
            if fnmatch.fnmatch(name, pattern):
                result.append(os.path.join(root, name))
    return result

# -----------------------------------------------------------
if __name__ == "__main__":

    #If a foldername is passed on the commandline, then look in this for the required files
    if len(sys.argv) == 2:
        fd = "./" + sys.argv[1]
    else:
        fd = "."
        if DEBUG:
            fd = "./sample"

    if os.path.isfile(f'{fd}/{training_file}'):
        # need to find the stub
        stubfile = findfile(stub_file, fd)
        if len(stubfile) == 1:
            main(f'{fd}/{training_file}', stubfile[0])
        elif len(stubfile) > 1:
            print(f"Sorry, found more than 1 {stub_file}")
        else:
            print(f"Sorry, I could not find the {stub_file} file")
            print("You can pass a directory on the commandline if the file is not located in the same directory")
    else:
        print(f"Sorry, I could not find the {training_file}")
        print("You can pass a directory on the commandline if the file is not located in the same directory")

    