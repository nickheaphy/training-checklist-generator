# Training Checklist Generator

Takes an Excel files and generates a HTML training checklist with dynamic options based on the data in the excel files.

## Data Files

There are two data files required. `Training.xlsx` and `*_stub.html`

### Excel Data File

The `Training.xlsx` spreadsheet contains all the data used to build the training checklist items. It contains three worksheets that need to be named `TrainingDays`, `MachineOptions` and `TrainingItems`

#### TrainingDays

This worksheet contains information about the training offered. There can be as many options are required.

There should be three columns. `ID` is used as a reference to this training item on the *TrainingItems* worksheet. `Description` is what is actually shown on the checklist once built. `Default` controls which options should be the default when the checklist is open (should be either Y or N)

In the sample data there is a *Standard Training*, *Extended Training* and *Advanced Training*.

#### MachineOptions

This worksheet contains information about hardware and software options that might be included in the training. This allows the content of the training to be tailored to the options sold to the customer (eg if the customer did not purchase an option, this should not be included in the training)

There should be three columns. `Group` allows logical grouping of options on the checklist. `ID` is used as a reference to this option on the *TrainingItems* worksheet. `Description` is what is actually shown on the checklist once built. `Default` controls which options should be the default when the checklist is open (should be either Y or N)

#### TrainingItems

This worksheet contains information about the individual training items. They are grouped into logical modules. Each row has a dependency on items on the *TrainingDays* and *MachineOptions* worksheets.

There should be four columns (additional columns are ignored). `Module Name` is the logical grouping of the individual training components (`Components` column).

The third column `Training Dependency` contains a space separated list of the `ID`s from the *TrainingDays*. This controls if this item is shown based on the training days selected in the final checklist. Eg you might want a module name to be common to two different trainings, but then the components might changed based on the training options selected. You can do this by putting both the training `ID`s in the module name row, then the individual training `ID`s in the component rows.

The fourth column `Machine Option Dependency` contains a space separated list of the `ID`s from the *MachineOptions* worksheet. This controls if this item is shown based on the machine options selected in the final checklist. Eg you might want a component to appear if any of multiple options are selected. You can do this by putting both the training `ID`s in the component rows.

There are some control characters in the module name. > indicates that this should be a new section and this is the section name. * indicates this is a subsection, and this is the subsection name.

In the second column (Components) a . indicates that this items should be indented (and two .. indents further)

### Stub File

There is also a `*_stub.html` file that contains the base format of the training checklist. This has CSS to control the formating etc.

Within the `*_stub.html` file there are placeholders that the generate.py script populates with the config checkboxes and the individual training items.

## Requirements

* Python 3

* OpenPYXL

### Installation (Mac)

Open a Terminal window.

Install [Brew](https://brew.sh/) as per the instructions on the website.

Once Brew is installed enter `brew install python3` in the terminal to install the latest Python version.

Once Python is installed, install the dependencies for this project `python3 -m pip install openpyxl`

Download the latest code [repository](https://bitbucket.org/nickheaphy/training-checklist-generator/get/master.zip)

## Using the generator

Create/Edit the `Training.xlsx` appropriate to the device.

Edit the `*_stub.html` and at minimum edit the `<title>` field to be appropriate for the machine. Rename the `*_stub.html` based on the machinename (but keep it ending in `_stub.html` otherwise the generate script won't be able to find it.)

Copy the `generate.py` script from the [repository](https://bitbucket.org/nickheaphy/training-checklist-generator/get/master.zip) into the folder with the `Training.xlsx` and `*_stub.html` files.

Run the command `python3 generate.py` - the script will look for the two files that it needs in the same directory and then build a new training document.

Copy the new `*.html` file from the folder - this is the training checklist.